package io.swagger.api.impl;

import io.swagger.api.NotFoundException;
import io.swagger.api.UsersApiService;
import io.swagger.model.UserDTO;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.ArrayList;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-03-22T11:34:46.953+01:00")
public class UsersApiServiceImpl extends UsersApiService {
    @Override
    public Response getUsers(SecurityContext securityContext) throws NotFoundException {
        List<UserDTO> users = new ArrayList<UserDTO>();
        for (int i = 0; i < 10; i++) {
            UserDTO user = new UserDTO();
            user.setId(i);
            user.setName("Teszt Elek " + i);
            users.add(user);
        }
        return Response.ok().entity(users).build();
    }
}
