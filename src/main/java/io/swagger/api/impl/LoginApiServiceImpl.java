package io.swagger.api.impl;

import io.swagger.api.LoginApiService;
import io.swagger.api.NotFoundException;
import io.swagger.model.LoginDTO;
import io.swagger.model.LoginResponseDTO;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-03-22T11:34:46.953+01:00")
public class LoginApiServiceImpl extends LoginApiService {
    @Override
    public Response login(LoginDTO loginDTO, SecurityContext securityContext) throws NotFoundException {
        LoginResponseDTO res = new LoginResponseDTO();
        res.setName(loginDTO.getUsername());
        res.setToken("dsadasdafda");
        return Response.ok().entity(res).build();
    }
}
