package io.swagger.api;

import io.swagger.model.LoginDTO;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-03-22T11:34:46.953+01:00")
public abstract class LoginApiService {
    public abstract Response login(LoginDTO loginDTO, SecurityContext securityContext) throws NotFoundException;
}
