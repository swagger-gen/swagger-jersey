package io.swagger.api;

import io.swagger.api.factories.UsersApiServiceFactory;
import io.swagger.model.UserDTO;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/users")
@Consumes({"application/json"})
@Produces({"application/json"})
@io.swagger.annotations.Api(description = "the users API")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-03-22T11:34:46.953+01:00")
public class UsersApi {
    private final UsersApiService delegate = UsersApiServiceFactory.getUsersApi();

    @GET

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @io.swagger.annotations.ApiOperation(value = "", notes = "", response = UserDTO.class, responseContainer = "List", tags = {"User",})
    @io.swagger.annotations.ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "user lista", response = UserDTO.class, responseContainer = "List")})
    public Response getUsers(@Context SecurityContext securityContext)
            throws NotFoundException {
        return delegate.getUsers(securityContext);
    }
}
