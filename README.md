# Swagger jaxrs (Jersey) generated server

```
java -jar swagger-codegen-cli-2.2.2.jar generate -i swagger.json -l jaxrs
```

jaxrs-resteasy-vel megegyező felépítés

Generálás utáni teendők:
* <packaging> átállítása war-ra jar-ról

Előnyök:
* referencia implementáció

Hátrányok:
* Sok függőség